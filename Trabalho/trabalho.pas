type
    vetor = array[0..15] of integer;

var
    v1: vetor;    

procedure initVet(var vet: vetor; size, init, incr: integer);
var i: integer;
begin
    i := 0;
    while (i < size) and (i < 16) do
    begin   
        vet[i] := init;
        //write(' ', vet[i], ' ');
        init := init + incr;
        i := i + 1;
    end;
end;

function somaVet(var fte: vetor; size: integer):integer;
var i, soma: integer;
begin
    i := 0;
    soma := 0;
    while (i < size) and (i < 16) do
    begin
        if (fte[i] < 0) then
            fte[i] := (-1) * fte[i];
        soma := soma + fte[i];
        i := i + 1;
    end;
    somaVet := soma;
end;

begin
    initVet(v1, 16, -13, 1);
    writeln(somaVet(v1, 16));
end.